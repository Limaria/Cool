﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cool
{
    class Program
    {
        static void Main(string[] args)
        {
            Archer value = new Archer("V", 10, 3);
            ShieldMan value1 = new ShieldMan("L", 8, 3);

            int m = 1;
            do
            {
                if (m % 2 == 0)
                {
                    value.Hit(value1);
                    value1.Log();
                    Console.ReadKey();
                }

                else
                {
                    value1.Hit(value);
                    value.Log();
                    Console.ReadKey();
                }
                m++;
            }
            while (value.health > 0 & value1.health > 0);
         }
    }

    abstract class Person 
    {
        public string name;
        public int health;
        public int damage;

        public Person(string name, int health, int damage)
        {
            this.name = name;
            this.health = health;
            this.damage = damage;
        }
        public Person(string name)
        {
            Random rnd = new Random();
            this.health = rnd.Next(200, 300);
            this.damage = rnd.Next(10, 16);
        }
        public void Hit<T>(T enemy) where T : Person
        {
            enemy.health -= this.damage;
        }

        public void Log()
        {
            Console.WriteLine($"{this.name} : {this.health}");
        }
    }
    class Archer : Person
    {
        int arrowsCount;

        int[][] arrows = new int[3][];
        int poisonedArrows = 0;
        int simpleArrows = 1;
        int explosiveArrows = 2;

        public Archer(string name, int health, int damage) : base (name, health, damage)
        {
            this.arrowsCount = 18;

            this.arrows[poisonedArrows] = new int[] { 10, 1 };
            this.arrows[simpleArrows] = new int[] { 5, 2 };
            this.arrows[explosiveArrows] = new int[] { 3, 3 };
        }
        
        public new void Hit<T>(T enemy) where T : Person
        {
            Random rnd = new Random();
            int ar = rnd.Next(arrows.Length);
            int[] hit = new int[1];


            if (enemy is ShieldMan s)
            {
                if (s.shield > 0)
                {
                    if (this.arrowsCount > 0)
                    {
                        this.arrowsCount--;
                        s.shield -= this.damage + rnd.Next(0, 2);
                    }
                    else
                    {
                        s.shield -= rnd.Next(0, 2);
                    }
                }
                else
                {
                    s.health -= this.damage;
                }
            }
            else
            {
                if (this.arrowsCount > 0)
                {
                    this.arrowsCount--;
                    enemy.health -= (this.damage + rnd.Next(2, 4));
                }
                else
                {
                    enemy.health -= rnd.Next(0, 2);
                }
            }
            
        }
    } 

    class ShieldMan : Person
    {
        public int shield;
        public ShieldMan(string name, int health, int damage) : base(name, health, damage)
        {
            Random rnd = new Random();
            this.shield = rnd.Next(3, 8);
        }

        public new void Hit<T>(T enemy) where T : Person
        {
            Random rnd = new Random();
            if (enemy is ShieldMan s)
            {
                if (s.shield > 0)
                {
                    s.shield -= this.damage;
                }
                else
                {
                    s.health -= this.damage;
                }
            }
            else
            {
                enemy.health -= rnd.Next(0, this.damage + 1);
            }
            
        }
    }

    class Magician : Person
    {
        public int mana;
        int[][] spells = new int[3][];
        int basin = 0;
        int fireBall = 1;
        int ironFist = 2;


        public Magician (string name, int health, int damage) : base(name, health, damage)
        {
            this.mana = 50;

            this.spells[basin] = new int[] {5, 2};
            this.spells[fireBall] = new int[] {10, 3};
            this.spells[ironFist] = new int[] {15, 4};
        }
        public new void Hit<T>(T enemy) where T : Person
        {
            Random rnd = new Random();
            int[] hit = new int[1];
            if (this.mana > 15)
            {
                hit = spells[rnd.Next(spells.Length)];
            }
            else if (this.mana > 10)
            {
                hit = spells[rnd.Next(spells.Length - 1)];
            }
            else if (this.mana > 5)
            {
                hit = spells[rnd.Next(spells.Length - 2)];
            }
            else
            {
                hit[0] = 0;
                hit[1] = damage;
            }
            if (enemy is ShieldMan s)
            {
                if (s.shield > 0)
                {
                    
                    this.mana -= hit[0];
                    s.shield -= hit[1];
                   
                }
                else
                {
                    s.health -= hit[1] + rnd.Next(3);
                }
            }
            else
            {
                this.mana -= hit[0];
                enemy.health -= hit[1];
            }
        }

    }
}
